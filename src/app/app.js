const Koa = require('koa');
const env = require('./envConf');
const app = new Koa();

const main = ctx => {
    console.log('currentEnv:%s', env.currentEnv);
    ctx.status = 200;
    ctx.body = env.currentEnv;
};

app.use(main);
app.listen(3000);
console.log('ready for service');