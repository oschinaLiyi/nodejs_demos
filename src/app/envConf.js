env = {
    //you can add default conf
};

if (process.env.NODE_ENV === 'dev') {
    env.currentEnv = 'dev';
    env.logLevel = 'debug';
} else if (process.env.NODE_ENV === 'prod1') {
    env.currentEnv = 'prod1';
    env.logLevel = 'info';
} else if (process.env.NODE_ENV === 'prod2') {
    env.currentEnv = 'prod2';
    env.logLevel = 'info';
}

console.log('env: %j', env);
module.exports = env;