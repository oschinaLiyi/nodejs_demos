const Redis = require('ioredis');

let redis = new Redis({
    port: 6379,
    host: 'localhost',  //如果不是本机地址，记得替换
    password: 'your_password',  //注意替换成自定义的密码
    connectTimeout: 100,
    maxRetriesPerRequest: 2,
    retryStrategy: function (times) {
        // reconnect after
        return Math.min(times * 10, 3000);
    },
    reconnectOnError: function (err) {
        let targetError = "READONLY";
        console.error('err:%j', err);
        if (err.message.slice(0, targetError.length) === targetError) {
            // Only reconnect when the error starts with "READONLY"
            return true; // or `return 1;`
        }
    }
});

redis.on('connect', err => {
    if (err) {
        console.error('failed to connect redis');
    } else {
        console.log('connected');
    }
});

redis.on('error', err => {
    if (err) {
        console.error('failed to start redis');
        // throw new Error('failed to start redis');
    } else {
        console.log('redis ready for service');
    }
});

redis.set('foo', 'bar', err => {
    console.error("error:", err);
    throw new Error('error occurred when set kv');
});

redis.get('foo', (err, result) => {
    console.log('get result:%j, err:%j', result, err);
});