const session = require('koa-generic-session');
const redisStore = require('koa-redis');
const Koa = require('koa');

const app = new Koa();
app.keys = ['keys', 'keykeys'];


let redis  = redisStore({
    port: 6379,
    host: 'localhost',
    password: 'your_password',
    connectTimeout: 100,
    maxRetriesPerRequest: 2,
    retryStrategy: function (times) {
        // reconnect after
        return Math.min(times * 10, 3000);
    },
    reconnectOnError: function (err) {
        let targetError = "READONLY";
        console.error('reconnect on error:%j', err);
        if (err.message.slice(0, targetError.length) === targetError) {
            // Only reconnect when the error starts with "READONLY"
            return true; // or `return 1;`
        }
    }
});


console.log('----');

redis.set('foo', 'bar');
redis.get('foo', (err, result) => {
    console.log('----e:%j, r:%j', err, result);
    if (err) {
        console.log("err get:%j", err);
    }else {
        console.log('get result:%j', result);
    }
});

app.use(session({
    store: redis
}));


app.use(function *() {
    switch (this.path) {
        case '/get':
            get.call(this);
            break;
        case '/remove':
            remove.call(this);
            break;
        case '/regenerate':
            yield regenerate.call(this);
            break;
    }
});

function get() {
    const session = this.session;
    session.count = session.count || 0;
    session.count++;
    this.body = session.count;
}

function remove() {
    this.session = null;
    this.body = 0;
}

function *regenerate() {
    get.call(this);
    yield this.regenerateSession();
    get.call(this);
}

app.listen(8080);