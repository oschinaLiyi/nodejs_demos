let log4js = require('log4js');
log4js.configure({
    "appenders": {
        "console": {
            "type": "console"
        },
        "info": {
            "type": "dateFile",
            "filename": "./logs/info",
            "encoding": "utf-8",
            "pattern": "yyyy-MM-dd-hh.log",
            "maxLogSize": 20000000,
            "alwaysIncludePattern": true,
            "layout": {
                "type": "pattern",
                "pattern": "[%d{ISO8601}][%5p  %z  %c] %m"
            },
            "compress": true,
            "keepFileExt": true,
            "daysToKeep": 30
        },
        "maxError": {
            "type": "logLevelFilter",
            "appender": "info",
            "level": "debug",
            "maxLevel": "error"
        },
        "error": {
            "type": "dateFile",
            "filename": "./logs/error",
            "pattern": "yyyy-MM-dd.log",
            "maxLogSize": 20000000,
            "encoding": "utf-8",
            "alwaysIncludePattern": true,
            "layout": {
                "type": "pattern",
                "pattern": "[%d{ISO8601}][%5p  %z  %c] %m"
            },
            "compress": true,
            "keepFileExt": true,
            "daysToKeep": 30
        },
        "minError": {
            "type": "logLevelFilter",
            "appender": "error",
            "level": "error"
        }
    },
    "categories": {
        "default": {
            "appenders": [
                "console",
                "maxError",
                "minError"
            ],
            "level": "trace"
        }
    }
});


const logger = log4js.getLogger('codeConfLog');

logger.trace('test trace');
logger.debug('test debug');
logger.info('test info');
logger.warn('test warn');
logger.error('test error');


