let log4js = require('log4js');
log4js.configure('src/log/log4js.json');

const logger = log4js.getLogger('fileConfLog');

logger.trace('test trace');
logger.debug('test debug');
logger.info('test info');
logger.warn('test warn');
logger.error('test error');

const getLogger = function(moduleName) {
    return log4js.getLogger(moduleName);
}

module.exports.getLogger = getLogger;